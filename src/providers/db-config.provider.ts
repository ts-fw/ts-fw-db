import {Provider} from 'ts-fw';
import {IDBConfig, DBConfig} from '../plugins';

@Provider('dbConfig')
export class DBConfigProvider {
    provide(): IDBConfig {
        return new DBConfig();
    }
}
