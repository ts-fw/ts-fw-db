import {DbModelsService} from '../services';

export function OnDBCreated(): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        Reflect.defineMetadata('OnDBCreated', propertyKey, constructor);
        if (DbModelsService.dbEventListeners.indexOf(constructor) === -1) {
            DbModelsService.dbEventListeners.push(constructor);
        }
    }
}
