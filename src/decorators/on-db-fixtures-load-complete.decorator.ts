import {DbModelsService} from '../services';

export function OnDBFixturesLoadComplete(): MethodDecorator {
    return (constructor: Function, propertyKey: string) => {
        Reflect.defineMetadata('OnDBFixturesLoadComplete', propertyKey, constructor);
        if (DbModelsService.dbEventListeners.indexOf(constructor) === -1) {
            DbModelsService.dbEventListeners.push(constructor);
        }
    }
}
