import {Injectable, ReflectUtils} from 'ts-fw';

export function Fixture(entry: Function, priority: number = 1): ClassDecorator {
    return (constructor: Function) => {
        Reflect.decorate([Injectable('Fixture')], constructor);
        Reflect.defineMetadata('entry', entry.name, constructor.prototype);
        Reflect.defineMetadata('priority', priority, constructor.prototype);
        ReflectUtils.annotateMethod(constructor);
    }
}
