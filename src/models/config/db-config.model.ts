import {RedisConfigModel} from './redis-config.model';
import {ORMConfigModel} from './orm-config.model';

export interface DBConfigModel {
    redis: RedisConfigModel;
    orm: ORMConfigModel;
}
