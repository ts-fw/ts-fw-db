import {ConnectionOptions} from 'typeorm';

export interface ORMConfigModel {
    options: ConnectionOptions;
    logging: boolean;
    fixtures: boolean;
}
