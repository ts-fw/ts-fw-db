import {ApplicationError} from 'ts-fw';

export class IncorrectFixtureError extends ApplicationError {
    constructor(entityName: string) {
        super(IncorrectFixtureError, `Fixture for ${entityName} method must be return a promise`);
    }
}
