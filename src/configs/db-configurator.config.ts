import * as winston from 'winston';
import {inject, Container} from 'inversify';
import {Config, IConfigStorage} from 'ts-fw';
import {IncorrectFixtureError} from '../errors';
import {IDBConfig} from '../plugins';
import {Logger, Connection, ConnectionOptions, Repository, getMetadataArgsStorage} from 'typeorm';
import {DBConfigModel, RedisConfigModel, ORMConfigModel} from '../models/config';
import {DbModelsService} from '../services';
import * as IORedis from 'ioredis';

@Config(2)
export class DBConfigurator {
    @inject('configStorage')
    private configStorage: IConfigStorage;

    @inject('logger')
    private logger: winston.Winston;

    @inject('dbConfig')
    private dbConfig: IDBConfig;

    @inject('di')
    private container: Container;

    @inject('dbModelsService')
    private dbModelsService: DbModelsService;

    async configure() {
        const dbConfig: DBConfigModel = this.configStorage.get<DBConfigModel>('db', {});

        if (!dbConfig.redis && !dbConfig.orm) {
            this.logger.error('DB and Redis configs not defined');
            return;
        }

        await this._configuringRedis(dbConfig.redis);
        await this._configuringORM(dbConfig.orm);
    }

    private async _configuringRedis(redisConfig: RedisConfigModel): Promise<void> {
        if (!redisConfig) {
            return;
        }

        try {
            const redis = await this.dbConfig.createRedisConnection(redisConfig);
            this.container.bind<IORedis.Redis>('redis').toConstantValue(redis);

            const services: any[] = this.dbModelsService.getDBEventListeners();

            for (const service of services) {
                const method = Reflect.getMetadata('OnRedisCreated', service);
                if (!method) {
                    continue;
                }

                await service[method](redis);
            }
        } catch (error) {
            this.logger.error(error);
        }
    }

    private async _configuringORM(ormConfig: ORMConfigModel): Promise<void> {
        if (!ormConfig) {
            return;
        }

        const models = this.dbModelsService.getModels();
        const subscribers = <Function[]>getMetadataArgsStorage().entitySubscribers
            .map(subscriber => subscriber.target);

        const connectionOptions: ConnectionOptions = {
            ...ormConfig.options,
            subscribers: subscribers,
            entities: models,
            logging: 'all',
            logger: <Logger>{
                logQuery: (query: string, parameters: any[]) => {
                    if (Array.isArray(parameters) && parameters.length !== 0) {
                        this.logger.debug(`Query: ${query}; Params: ${parameters}`);
                        return;
                    }

                    this.logger.debug(`Query: ${query}`);
                },
                logQueryError: (error: string, query: string, parameters: any[]) => {
                    if (Array.isArray(parameters) && parameters.length !== 0) {
                        this.logger.debug(`Error: ${error}; Query: ${query}; Params: ${parameters}`);
                        return;
                    }

                    this.logger.debug(`Error: ${error}; Query: ${query}`);
                },
                logQuerySlow: (time: number, query: string, parameters: any[]) => {
                    if (Array.isArray(parameters) && parameters.length !== 0) {
                        this.logger.debug(`Slow query: ${query}; Params: ${parameters}; Time: ${time}`);
                        return;
                    }

                    this.logger.debug(`Slow query: ${query}; Time: ${time}`);
                },
                logSchemaBuild: (message: string) => {
                    this.logger.debug(`Build: ${message}`);
                },
                logMigration: (message: string) => {
                    this.logger.debug(`Migration: ${message}`);
                },
                log: (level: 'log' | 'info' | 'warn', message: any) => {
                    if (level === 'log') {
                        this.logger.debug(`TypeORM: ${message}`);
                    } else if (level === 'info') {
                        this.logger.info(`TypeORM: ${message}`);
                    } else {
                        this.logger.warn(`TypeORM: ${message}`);
                    }
                }
            }
        };

        try {
            const connection = await this.dbConfig.createORMConnection(connectionOptions);
            this.container.bind<Connection>('dbConnection').toConstantValue(connection);

            const onDBCreatedServices: Function[] = this.dbModelsService.getDBEventListeners();

            for (const model of models) {
                const modelName = model.name.charAt(0).toLowerCase() + model.name.slice(1);
                const modelRepository = connection.getRepository(model);
                this.container.bind<Repository<any>>(`${modelName}Repository`).toConstantValue(modelRepository);
                this.logger.debug(`${modelName}Repository was registered`);
            }

            for (const service of onDBCreatedServices) {
                const method = Reflect.getMetadata('OnDBCreated', service);
                if (!method) {
                    continue;
                }

                await service[method](connection);
            }

            if (!ormConfig.fixtures) {
                return;
            }

            await this._loadDBFixtures(models);

            const onDBFixturesLoadCompleteServices: Function[] = this.dbModelsService.getDBEventListeners();

            for (const service of onDBFixturesLoadCompleteServices) {
                const method = Reflect.getMetadata('OnDBFixturesLoadComplete', service);
                if (!method) {
                    continue;
                }

                await service[method]();
            }
        } catch (error) {
            this.logger.error(error);
        }
    }

    private async _loadDBFixtures(models: Function[]): Promise<void> {
        const fixtures: any[] = [];
        try {
            const tempFixtures = this.container.getAll('Fixture');
            fixtures.push(...tempFixtures.sort((first: any, second: any) => {
                const firstPriority: number = Reflect.getMetadata('priority', first);
                const secondPriority: number = Reflect.getMetadata('priority', second);
                if (firstPriority < secondPriority) {
                    return -1;
                }

                if (firstPriority > secondPriority) {
                    return 1;
                }

                return 0;
            }));
        } catch (ignore) {
        }

        for (const fixture of fixtures) {
            const model: any = models.find((model: any) => {
                return Reflect.getMetadata('entry', fixture) === model.name;
            });

            if (!model) {
                this.logger.debug(`Model for fixtures loader ${fixture.constructor.name} not found`);
                continue;
            }

            const method: string = Reflect.getMetadata('method', fixture);
            try {
                const result = fixture[method]();
                if (!result || !result.then && !result.catch) {
                    throw new IncorrectFixtureError(model.name);
                }

                await result;
                this.logger.debug(`Fixtures for ${model.name} was loaded`);
            } catch (error) {
                this.logger.error(error);
            }
        }
    }
}
