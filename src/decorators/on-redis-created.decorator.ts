import {DbModelsService} from '../services';

export function OnRedisCreated(): MethodDecorator {
    return (constructor: Function, propertyKey: string) => {
        Reflect.defineMetadata('OnRedisCreated', propertyKey, constructor);
        if (DbModelsService.dbEventListeners.indexOf(constructor) === -1) {
            DbModelsService.dbEventListeners.push(constructor);
        }
    }
}
