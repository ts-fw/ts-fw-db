import {IDBConfig} from './db-config.interface';
import {Connection, ConnectionOptions, createConnection} from 'typeorm';
import * as IORedis from 'ioredis';

export class DBConfig implements IDBConfig {
    async createRedisConnection(redisOptions: IORedis.RedisOptions): Promise<IORedis.Redis> {
        return new IORedis(redisOptions);
    }

    createORMConnection(connectionOptions: ConnectionOptions): Promise<Connection> {
        return createConnection(connectionOptions);
    }
}
