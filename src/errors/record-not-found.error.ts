import {ApplicationError} from 'ts-fw';

export class RecordNotFoundError extends ApplicationError {
    constructor(message: string) {
        super(RecordNotFoundError, message, 404);
    }
}
