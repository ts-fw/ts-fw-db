import {Service} from 'ts-fw';

@Service()
export class DbModelsService {
    static readonly models: Function[] = [];

    static readonly dbEventListeners: any[] = [];

    getModels(): Function[] {
        return DbModelsService.models;
    }

    getDBEventListeners(): Function[] {
        return DbModelsService.dbEventListeners;
    }
}
