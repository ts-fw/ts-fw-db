import {ApplicationError} from 'ts-fw';

export class RecordDuplicateError extends ApplicationError {
    constructor(message: string) {
        super(RecordDuplicateError, message, 400);
    }
}
