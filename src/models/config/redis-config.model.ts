import * as IORedis from 'ioredis';

export interface RedisConfigModel extends IORedis.RedisOptions {
}
