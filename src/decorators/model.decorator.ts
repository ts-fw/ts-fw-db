import {Entity} from 'typeorm';
import {DbModelsService} from '../services';

export function Model(): ClassDecorator {
    return (target: any) => {
        Reflect.decorate([<ClassDecorator>Entity()], target);
        DbModelsService.models.push(target);
    }
}
