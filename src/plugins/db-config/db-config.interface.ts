import {Connection, ConnectionOptions} from 'typeorm';
import * as IORedis from 'ioredis';

export interface IDBConfig {
    createORMConnection(connectionOptions: ConnectionOptions): Promise<Connection>;
    createRedisConnection(redisOptions: IORedis.RedisOptions): Promise<IORedis.Redis>;
}
